/**
 * @author adnan
 * @version 1.0
 * Java's built in Stack class does not have any limitation on its size. We are
 * creating a class LimitedStack that has Stack class but limits it by a 
 * maximum size. 
 */
package category02.datastructure;

public class LimitedStack<T> extends Stack<T>  {

	private int size;
	private int nItems;
	
	public LimitedStack(int size){
		super();
		this.size = size;	
		nItems = 0;
	}
	
	@Override
	public void push(T elem){
		if(nItems < size) {
			super.push(elem);
			nItems++;
		}
	}
	
	@Override
	public T pop(){
		if (nItems>0){
			nItems--;
			return super.pop();
		}
		return null;		
	}
}
