package category02.datastructure;

import org.junit.Test;

class Stack1 {
	int data[];
	int top;
	
	public Stack1(int size){
		data = new int[size];
		top = 0;
	}
	
	public void push(int x){		
		if(data!= null && top<data.length)
			data[top++] = x;
		
	}
	
	public int pop(){
		if(data!=null && top>0)
			return data[--top];
		else return -1;
	}
	
	public void display(){
		for(int i=0;i<top;i++)
			System.out.print(data[i] + " ");
		System.out.println("");
	}
	

}

public class StackApp{
	
	public StackApp(){
		
	}
	
	@Test
	public void testStack(){
		
		Stack1 s = new Stack1(3);
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		
		s.display();
		
		s.pop();
		s.display();
		
		
		//3 pops to follow 
		s.pop();
		s.pop();
		s.pop();
		
	}

}
