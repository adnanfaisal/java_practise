package category10.jvm.recursion;

/**
 *  From Gayle's book, Chapter Recursion, Exercise#1
 * @author adnan
 */
import static org.junit.Assert.*;

import org.junit.Test;

public class StepsOfStaircase {

	// My solution: good but without DP
	public int getWays(int nStep, int sum){
		if(sum == nStep) {
			return 1;
			
		}
		if (sum > nStep) return 0;
		return getWays(nStep, sum + 1) + 
				getWays(nStep, sum + 2) +
				getWays(nStep, sum + 3);
	}
	
	// My solution: worse and without DP
	public int getWays1(int nStep, int sum, int nWays){
		if(sum == nStep) {
			return ++nWays;
			
		}
		if (sum > nStep) return 0;
		return getWays1(nStep, sum + 1, nWays) + 
				getWays1(nStep, sum + 2, nWays) +
				getWays1(nStep, sum + 3, nWays);
	}

	// Gayle's solution without DP
	public int getWays2(int nStep){
		if(nStep <0) return 0;
		if (nStep == 0) return 1;
		return getWays2(nStep - 1) + 
				getWays2(nStep - 2) +
				getWays2(nStep - 3);
	}
	
	
	@Test
	public void test1(){		
//		assertEquals(81,getWays(8,0));
//		assertEquals(81,getWays1(8,0,0));
//		assertEquals(81,getWays2(8));
		
		
	}
	
}
