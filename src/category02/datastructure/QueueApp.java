package category02.datastructure;

import org.junit.Test;

class Queue{
	int data[];
	int front = 0;
	int rear = -1;
	int nItems = 0;
	
	public Queue(int size){
		data = new int[size];
	}
	
	public void insert(int value) throws Exception{		
		if (nItems == data.length){
			throw new Exception("Queue overflow");
		}
		else{
			rear = (rear + 1) % data.length;
			data[rear] = value;
			nItems++;
			System.out.println(value + " inserted.");
		}
	}
	
	public int remove() throws Exception{
		if(nItems == 0) {
			throw new Exception("Queue is empty");
		}
		int temp = data[front];
		front = (front + 1) % data.length;
		nItems--;
		System.out.println(temp + " removed.");
		return temp;
	}
	
	public void display(){
		
		for(int count = 0, i = front; count < nItems; count++){
			System.out.print(data[i] + " ");
			i = (i + 1) % data.length;
		}
		System.out.println("");
	}
	
	public boolean isFull(){
		
		if (nItems == data.length) return true;
		else return false;
	}
	
}
public class QueueApp {
	
	public QueueApp(){}
	
	@Test
	public void QueueTest() {
		Queue queue = new Queue(3);
		try{
			queue.insert(10);
			queue.insert(20);
			queue.insert(30);
		}catch(Exception ex){
			ex.printStackTrace();
		}
			queue.display();
		try{
		queue.insert(40);
		queue.display();
		}catch(Exception ex) { System.err.println(ex);}
		
		try{
		queue.remove();
		queue.display(); 
		} catch(Exception ex) { System.err.println(ex);}
		
		try{
		queue.remove();
		queue.remove();
		queue.remove();
		queue.display();
		} catch(Exception ex) { System.err.println(ex);}
		
		
	}

}
