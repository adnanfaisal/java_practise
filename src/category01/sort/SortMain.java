package category01.sort;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

public class SortMain {

	@Test
	public void testSortArray(){
	
		int []given = {1,-1,5,10,-40};
		int []sorted = {-40,-1,1,5,10};
		
		Arrays.sort(given);
		assertArrayEquals(given,sorted);				
	}
	
	@Test
	public void testSortArrayListOfString(){
		ArrayList<String> given = new ArrayList<String>();
		given.add("Hello");
		given.add("Adnan");
		given.add("Faisal");
		given.add("Janab");		
		Collections.sort(given);
		
		
		ArrayList<String> sorted = new ArrayList<String>();
		sorted.add("Adnan");
		sorted.add("Faisal");
		sorted.add("Hello");
		sorted.add("Janab");
	
		
		assertEquals(given,sorted);
	}
	@Test
	public void testSortArrayListOfBook(){
		ArrayList<Book> given = new ArrayList<Book>();
		given.add(new Book(3,"java","sheildt",100.5));
		given.add(new Book(1,"c++","kanitkar",50.5));
		given.add(new Book(2,"pascal","jayasuriya",30.5));
					
		Collections.sort(given);
		for(Book b:given)
			System.out.println(b);
		
		Collections.sort(given,new SortBookByPrice());
		for(Book b:given)
			System.out.println(b);
		
		ArrayList<Book> sorted = new ArrayList<Book>();
		sorted.add(new Book(3,"java","sheildt",100.5));
		sorted.add(new Book(2,"pascal","jayasuriya",30.5));
		sorted.add(new Book(1,"c++","kanitkar",50.5));
	
		
		assertEquals(given,sorted);
	}
}
