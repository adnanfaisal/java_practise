package category01.sort;

import java.util.ArrayList;
import java.util.List;

public class QuickSort {

	public static List<Integer> quickSort(List<Integer> input) {
		
		if(input.size()<2) return input;
		
		final Integer pivot = input.get(0);
		final List<Integer> lower = new ArrayList<Integer>();
		final List<Integer> higher = new ArrayList<Integer>();
		
		// start from index no. 1 since pivot is the 0'th element 
		for(int i = 1;i<input.size();i++){
			if (input.get(i)<pivot) lower.add(input.get(i));
			else higher.add(input.get(i));
		}
		
		final List<Integer> sortedList = quickSort(lower);
		sortedList.add(pivot);
		sortedList.addAll(quickSort(higher));
		return sortedList;		
	}
	
	public static void main(String []args){
		ArrayList<Integer> values = new ArrayList<Integer>();
		values.add(10);
		values.add(-9);
		values.add(50);
		values.add(3);
		ArrayList<Integer> sortedValues = (ArrayList<Integer>) quickSort(values);
		for(Integer in:sortedValues)
			System.out.print(in+ " ");
		
	}
}
