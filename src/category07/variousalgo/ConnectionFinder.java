package category07.variousalgo;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

import org.junit.Test;

/**
 * This class finds if in Graph G, node b is reachable from node a
 * @author adnan
 *
 */
public class ConnectionFinder {

	Graph graph;
	Queue<Character> queue = new ArrayDeque<>();
	boolean []visited;
	char []parent;

	
	
	public void setGraph(Graph g){
		graph = g;
		visited = new boolean[g.getNVertex()];
		parent = new char[g.getNVertex()];
	
	}
	
	/**
	 * This method uses BFS approach for finding connection. 
	 * A DFS based approach could have been also used
	 * @param a source node
	 * @param b destination node 
	 * @return true if node b is reachable from node a, false otherwise
	 */
	public boolean hasConnection(char a, char b){
	
		a = Character.toUpperCase(a);
		b = Character.toUpperCase(b);
		

		
		
		if(b==a) return true;
		int aInt = Graph.getIntArrayIndexFromChar(a);
		
		visited[aInt] = true;
		queue.add(a);
		
		while(! queue.isEmpty()){
			char j = queue.poll();			
			List<Character> adjacent = graph.getAdjacent(j);
			for(Character c:adjacent){
	//			System.out.println("Adjacent of " + j + " is " + c);
				int cInt = Graph.getIntArrayIndexFromChar(c);
				if(visited[cInt] != true){				
					visited[cInt] = true;
					parent[cInt] = j;
					if(c==b) {
						System.out.println(getPath(a,b));
						return true;
					}
					queue.add(c);			
				}
			}
		}
		return false;		
	}
	
	/**
	 * 
	 * @param a source node
	 * @param b destination node
	 * @return path from source to destination node
	 */
	private String getPath(char a, char b){
		
		StringBuilder sb = new StringBuilder();
		int aInt = Graph.getIntArrayIndexFromChar(a);
		int bInt = Graph.getIntArrayIndexFromChar(b);
		int current = bInt; // start backwards
		while(current != aInt){
			char currentChar = Graph.getCharFromInt(current);
			sb.append(currentChar + " ");
			current = Graph.getIntArrayIndexFromChar(parent[current]);
		}
		
		sb.append(a);
		return new String(sb.reverse());		
		
	}
	
	@Test
	public void isPathFromAtoB1(){
		
	
		Graph graph = new Graph(5,true);
		graph.addEdge('a', 'b');
		graph.addEdge('a', 'c');
		graph.addEdge('b', 'd');
		graph.addEdge('c', 'd');
		graph.addEdge('d', 'e');
		
		ConnectionFinder cf = new ConnectionFinder();
		cf.setGraph(graph);
		
		
		System.out.println(graph);
		assertTrue(cf.hasConnection('a','e'));		
		assertFalse(cf.hasConnection('e','a'));
		
	}
	
	/**
	 *  Example taken from venugopal's lect-1 of bfs (youtube)
	 * This graph is shown in 9:42 min
	 */
	@Test
	public void isPathFromAtoB2(){
		
		Graph graph = new Graph(7,true);
		graph.addEdge('a', 'b');
		graph.addEdge('a', 'c');
		graph.addEdge('b', 'd');
		graph.addEdge('c', 'd');
		graph.addEdge('d', 'e');
		
		graph.addEdge('b', 'e');
		graph.addEdge('d', 'f');
		graph.addEdge('f', 'g');
		graph.addEdge('a', 'g');
		graph.addEdge('g', 'c');
		
		
		ConnectionFinder cf = new ConnectionFinder();
		cf.setGraph(graph);
		
		
		System.out.println(graph);
		assertTrue(cf.hasConnection('a','f'));		
		assertFalse(cf.hasConnection('d','a'));
		
	}
}
