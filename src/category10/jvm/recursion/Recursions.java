package category10.jvm.recursion;
//test comment 2
import java.util.ArrayList;
import java.util.List;

public class Recursions {

	// version 1, without local variable
	public static int fibonacci1(int n){
		if(n==1) return 1;
		else return (n * fibonacci1(n-1));
	}
	
	// version 2, with local variable
	public static int fibonacci2(int n){
		if(n==1) return 1;
		else {
			int fibo = n;
			fibo = fibo * fibonacci1(n-1);
			return fibo;
		}
	}
	
	// compare this code with fibonnaci2
	public static List<Integer> reverseList(List<Integer> list){
		if(list.size() == 1) return list;
		else {
			List<Integer> reversed = new ArrayList<Integer>();
			reversed.add(list.get(list.size()-1));
			reversed.addAll(reverseList(list.subList(0, list.size()-1)));
			return reversed;
		}
	}
	
	
	private int findTriangular(int n){
		if(n==1) return 1;
		else return n + findTriangular(n-1);		
	}
	
	
	// just adding some more comments
	public static void main(String args[]){
		
     	System.out.println("The fibonacci number is: "  + fibonacci1(5));
		System.out.println("The fibonacci number is: " + fibonacci2(5));


		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(10);
		list.add(20);
		list.add(30);
		
		System.out.println("The fibonacci numbers are: " +list.toString());		
		list = reverseList(list);
		System.out.println("The fibonacci numbers are: " + list.toString());
		
	}
}
