package category06.designpattern.strategypattern;

public class Dog extends Animal {

	public Dog(String name) {
		super(name);
		flyType = new CantFly();
	}
}
