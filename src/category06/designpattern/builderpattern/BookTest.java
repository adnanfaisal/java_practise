package category06.designpattern.builderpattern;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookTest{

@Test
public void fictionBookGood(){
	Book.Builder bookBuilder = new Book.Builder();
	Book book1 = bookBuilder.withName("Sherlock Holmes")
				.withAuthor("Sir. A. C. Doyale")
				.withPrice(10.0)
				.build();
				
	assertEquals(book1.getName(),"Sherlock Holmes");
	assertEquals(book1.getType(),BookType.FICTION);
}

@Test(expected = IllegalStateException.class) 
public void fictionBookAuthorMissing(){
	Book.Builder bookBuilder = new Book.Builder();
	Book book1 = bookBuilder.withName("Sherlock Holmes")
				.withBookType(BookType.NON_FICTION)
				.withPrice(10.0)
				.build();
				

	assertEquals(book1.getType(),BookType.NON_FICTION);
}

}