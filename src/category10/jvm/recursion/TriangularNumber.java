package category10.jvm.recursion;

import java.util.Scanner;

import org.junit.Test;

/*
 * Triangular numbers are 1, 3, 6,  10, 15, 21, 28 etc. 
 */

public class TriangularNumber {

	private int findTriangular(int n){
		if(n==1) return 1;
		else return n + findTriangular(n-1);		
	}
	
	
	@Test
	public void go(){
		Scanner in;
		while (true){
			in = new Scanner(System.in);
			int num = in.nextInt();
			if(num <= 0) {
				in.close();
				break;
			}
			else 
				System.out.println(findTriangular(num));
		
		}
		
	}	
	
	public static void main(String args[]){		
	}
	
}
