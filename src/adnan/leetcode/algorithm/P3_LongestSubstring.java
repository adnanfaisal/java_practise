package adnan.leetcode.algorithm;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

public class P3_LongestSubstring {
	public int lengthOfLongestSubstring(String s) {
        Queue<Character> queue = new LinkedList<>();// If you use ArrayList or ArrayDeque, your time limit will exceed
        int maxLength = 0;
    
  
        for(int i = 0;i < s.length();i++){
            
            if( !queue.contains(s.charAt(i)) ){
                queue.add(s.charAt(i));
            }
            else{
                    if (queue.size() > maxLength) maxLength = queue.size();
                
                    queue.add(s.charAt(i));
                    
                    while(queue.peek() != s.charAt(i))
                        queue.poll();
                    queue.poll(); // remove the recurring char
            }
        }
        
        // to check the case where the longest sequence ends with a null, such as "abc"
        if (queue.size() > maxLength) maxLength = queue.size();
        
        return maxLength;
    }
	
	@Test
	public void test1(){
		int len =new P3_LongestSubstring().lengthOfLongestSubstring("abcabcbb");
		assertEquals(len,3);
	}
}
