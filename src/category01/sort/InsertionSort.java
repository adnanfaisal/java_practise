package category01.sort;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InsertionSort {
	
	public static List<Integer> insertionSort(List<Integer> input){
		List<Integer> output = new LinkedList<Integer>();
		
		outerloop: for(Integer in:input){
			for(int i = 0; i< output.size();i++){
				Integer out = output.get(i);
				if (in<out) {
					output.add(i, in);
					continue outerloop;
				}
			}
			output.add(output.size(),in);
		}
		
		return output;
		
	}

	public static void main(String []args){
		ArrayList<Integer> values = new ArrayList<Integer>();
		values.add(10);
		values.add(-9);
		values.add(50);
		values.add(3);
		LinkedList<Integer> sortedValues = (LinkedList<Integer>) insertionSort(values);
		for(Integer in:sortedValues)
			System.out.print(in+ " ");
		
	}
}
