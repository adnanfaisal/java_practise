package category06.designpattern.strategypattern;

public interface FlyBehaviour {

	public String fly();
}

class ItFlies implements FlyBehaviour{

	@Override
	public String fly() {
		return "I fly";
	}	
}

class CantFly implements FlyBehaviour{

	@Override
	public String fly() {
		return "I can't fly";
	}	
}