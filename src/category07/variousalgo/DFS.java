package category07.variousalgo;


import java.util.List;


public class DFS {

	Graph graph;
	boolean visited[];
	
	public  DFS(int nVertex){
		graph = new Graph(nVertex);
		visited = new boolean[nVertex];		
	}
	
	public String doDepthFirstSearch(){
		return doDepthFirstSearch('A', new StringBuilder());
	}
	
	public String doDepthFirstSearch(Character src){
		return doDepthFirstSearch(src, new StringBuilder());
	}
	/*
	 * @param src the node from where traversal begins
	 * @return the list of nodes visited as a String
	 */
	public String doDepthFirstSearch(Character src, StringBuilder sb){
		
		int srcInt = Graph.getIntArrayIndexFromChar(src);
		visited[srcInt] = true;
		
		if(src!=null)
		{ 
		
			sb.append(Character.toUpperCase(src) + " ");	
			List<Character> adjacent = graph.getAdjacent(src);		
			for (char c:adjacent){
				if(visited[Graph.getIntArrayIndexFromChar(c)]!= true){
					System.out.println("Adjacent of  " +  Character.toUpperCase(src) + " is " + c);
					srcInt = Graph.getIntArrayIndexFromChar(c);
					doDepthFirstSearch(c,sb);
				}
			}
		}
		return new String(sb);
		
	}
	
	
	
	public Graph getGraph() {
		return graph;
	}

}
