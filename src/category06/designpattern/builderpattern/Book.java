package category06.designpattern.builderpattern;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * A class to demonstrate the use of the builder pattern
 * @author adnan
 * @version 1.0
 */
public class Book {

///////////////////////////////////////////////////////////////////
/// Member variables 
////////////////////////////////////////////////////////////////////
	private final String name; // must provide a value
	private final String author; // must provide a value
	private BookType type = BookType.FICTION; // optional, has a default value
	private final double price;  // optional
	
////////////////////////////////////////////////////////
//  Builder Inner Class
/////////////////////////////////////////////////////////
	public static class Builder{
		private String name;
		private String author;
		private BookType type;
		private double price;
		
		public Builder withName(String name){
			this.name = name;
			return this;
		}
		
		public Builder withAuthor(String author){
			this.author = author;
			return this;
		}
		public Builder withBookType(BookType bookType){
			this.type = bookType;
			return this;
		}
		
		public Builder withPrice(double Price){
			this.price = price;
			return this;
		}
		
		public Book build(){
			if(name == null || author == null) throw new IllegalStateException("Cannot create book.");
			return new Book(name,author,type,price);
		}		
	}
/////////////////////////////////////////////////////////////////////////////
// Methods of the Book class
////////////////////////////////////////////////////////////////////////////	

private Book(final String name, final String author, final BookType type, final double price) {
	super();
	this.name = name;
	this.author = author;
	this.type = type;
	this.price = price;
}

public String getName() {
	return name;
}

public String getAuthor() {
	return author;
}

public BookType getType() {
	return type;
}

public double getPrice() {
	return price;
}


}
