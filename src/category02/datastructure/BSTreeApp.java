package category02.datastructure;

import static org.junit.Assert.*;

import org.junit.Test;

class Node  {
	int value;
	Node left;
	Node right;
	
	public Node(int value){
		this.value = value;
		left = right = null;
	}


}

class BinarySearchTree {
	Node first;
	
	public BinarySearchTree(int value){
		first = new Node(value);		
	}
	
	public Node getRoot(){
		return first;
	}
	public void add(int value)
	{
		addRecursive(first, value);
	}
	
	private void addRecursive(Node current, int value){
		if(value <= current.value)
			if (current.left == null){
				Node newNode = new Node(value);
				current.left = newNode;
			}
			else addRecursive(current.left,value);
		else
			if(current.right == null){
				Node newNode = new Node(value);
				current.right = newNode;
			}
			else addRecursive(current.right,value);					
	}
	
	public boolean find(int value){
		
		if (first == null) return false;
		else return find(value,first);
	}
	
	private boolean find(int value, Node current){
		if(current == null) return false;
		else if (value == current.value) return true;
		else if(value < current.value) return find(value,current.left);
		else return find(value,current.right);
	}
	

	
	public void displayTreePreOrder(Node current){
		if (current != null){
			System.out.print(current.value + " ");
			displayTreePreOrder(current.left);
			displayTreePreOrder(current.right);
		}
	}
	
	public void displayTreePostOrder(Node current){
		if (current != null){			
			displayTreePostOrder(current.left);
			displayTreePostOrder(current.right);
			System.out.print(current.value + " ");
		}
	}
	
	public void displayTreeInOrder(Node current){
		if (current != null){			
			displayTreeInOrder(current.left);
			System.out.print(current.value + " ");
			displayTreeInOrder(current.right);
		}
	}
	
	public int sumLeafNodes(){
		return sumLeaves(first);
		//return addLeaves2(first);
	//	return addLeaves4(first);
		
	}
	
    /* I got this one from net */
	/* Very important recursion: a recursion that returns value */
	/* Lesson: addition / mult have to be done during recursive calls */
	/* Lesson: the "else" of ( != || != ) is ( == && == )*/
	int sumLeaves(Node current){			
		if(current !=null){		
			if(current.left != null || current.right != null) 
				return sumLeaves(current.left) + sumLeaves(current.right);		
			else 
				return current.value;	/* breaking condition simply returns a value */
		}
		else return 0;
	}
	
	/*
	 * I got it from net and think THIS IS THE BEST
	 */
	int sumLeaves0(Node current){
		if(current == null) return 0;
		else if (current.left == null && current.right == null)
			return current.value;
		return (sumLeaves0(current.left) + sumLeaves0(current.right));
		
	}
	
	/* worse way by me. but it seems to be working. */
	int sumLeaves2(Node current){
	
		if(current.left == null && current.right == null)
			return current.value;
		else if (current.left != null && current.right != null)
			return sumLeaves2(current.left) + sumLeaves2(current.right);	
		else if (current.left != null && current.right == null)
			return sumLeaves2(current.left);
		else
			return sumLeaves2(current.right);	
	}
	

	// By me on 22 Sept
	public int sumLeaves4(Node root){
		if(root == null) return 0;
		else if (root.left != null || root.right != null)
			return sumLeaves4(root.left) + sumLeaves4(root.right);
		else return root.value;
		
	}
	
	/**
	 * @author adnan
	 * @since 26 Sept 2016
	 * @param node The root node of the Binary Search Tree
	 * @returns height of the binary search tree 
	 */
	public int findHeight(Node node){
		if (node == null) return 0;
		if(node.left == null && node.right == null) return 0; // comment out this line if you want to start level number from 1		
		return 1 + Math.max(findHeight(node.left), findHeight(node.right));
	}
	
	public boolean isBinary(){
		return isBinary(first, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	// Time and space complexity O(n) as we are visiting every node
	private boolean isBinary(Node localRoot, int min, int max){
		if (localRoot == null) return false;
		if (! (localRoot.value > min && localRoot.value < max))
			return false;
		if(localRoot.left != null) return isBinary(localRoot.left,min,localRoot.value);
		if(localRoot.right != null) return isBinary(localRoot.right,localRoot.value,max);
		return true;
	}
}
public class BSTreeApp {

	public BSTreeApp(){}
	
	@Test
	public void BinarySearchTreeTest(){
		
	// See Lafore page 365 for the following tree	
	BinarySearchTree bsTree = new BinarySearchTree(53);
	bsTree.add(30);
	bsTree.add(72);
	bsTree.add(14);
	bsTree.add(39);
	bsTree.add(61);
	bsTree.add(84);
	bsTree.add(9);
	bsTree.add(23);	
	bsTree.add(34);
	bsTree.add(47);
	bsTree.add(79);
	

	
	assertEquals (253,bsTree.sumLeafNodes());
//	assertEquals (253,btree.sumLeaves2(btree.first));
//	assertEquals (253, btree.sumLeaves4(btree.first));
	
	assertEquals(bsTree.findHeight(bsTree.getRoot()),3);
	
	assertFalse(bsTree.find(139));
	assertTrue(bsTree.find(47));
	assertTrue(bsTree.isBinary());
	
	System.out.print("In order: "); // left -> center -> right
	bsTree.displayTreeInOrder(bsTree.getRoot()); // prints in ascending order
	System.out.println("");
	
	
	System.out.print("Pre order: "); // center -> left -> right 
	bsTree.displayTreePreOrder(bsTree.getRoot());
	System.out.println("");
	
	System.out.print("Post order: "); // left -> right -> center
	bsTree.displayTreePostOrder(bsTree.getRoot());
	System.out.println("");
	
	}
}
