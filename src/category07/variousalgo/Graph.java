package category07.variousalgo;

import java.util.LinkedList;
import java.util.List;



/**
 * 
 * @author adnan
 * @version 03-10-2016
 * 
 * This class models a graph using adjacent matrix.
 * Assumption: the nodes are marked in alphabetical order without skipping. For
 * example: If a graph has 3 nodes they are A,B and C. They Can't be A, B and D.
 */

public class Graph {

	private int adjacentMatrix[][];
	private int nVertex; // no. of rows and columns
	private boolean isDirected; // default = false
	
	
	public boolean isDirected() {
		return isDirected;
	}

	public void setDirected(boolean isDirected) {
		this.isDirected = isDirected;
	}

	public Graph(int nVertex){
		this(nVertex,false);
	}
	
	public Graph(int nVertex, boolean isDirected){
		this.nVertex = nVertex;
		adjacentMatrix = new int[nVertex][nVertex];
		this.isDirected = isDirected;
	}
	
	public static int getIntArrayIndexFromChar(char ch){
		int charMadeInt =  (Character.toUpperCase(ch) - 'A');
		return charMadeInt;
	}
	
	
	public void addEdge(char src, char dest){
		int srcUpper =  getIntArrayIndexFromChar(src);
		int destUpper =  getIntArrayIndexFromChar(dest);;
		
		if(!(inRange(srcUpper,0,nVertex)))
			throw new IllegalArgumentException();
		
		if(!(inRange(destUpper,0,nVertex)))
			throw new IllegalArgumentException();
		
		
		adjacentMatrix[srcUpper][destUpper] = 1;
		if(!isDirected) 
			adjacentMatrix[destUpper][srcUpper] = 1;
		
	}
	
	public boolean inRange(int n, int low, int high){
		if(! (n>=low && n<high)) return false;
		return true;
	}
	
	@Override
	public String toString(){
		
		StringBuilder sb = new StringBuilder();		
		
		// Build the title row 
		sb.append("  ");
		for(int j=0;j<nVertex;j++){
			sb.append(getCharFromInt(j));
			sb.append(' ');
		}
		
		sb.append('\n');
			
		// build the matrix
		for(int i=0;i<nVertex;i++){
			for(int j=0;j<nVertex;j++){
				if(j==0) {
					sb.append(getCharFromInt(i));
					sb.append(' ');
				}
				sb.append(adjacentMatrix[i][j]);
				sb.append(' ');
			}
			sb.append('\n');
		}
		
		String theMatrix = new String(sb);
		return theMatrix;
		
	}
	
	public static char getCharFromInt(int j){
		char vertex = (char) (j + 'A');
		return vertex;		
	}
	
	public int getNVertex(){
		return nVertex;
	}
	/**
	 * 
	 * @param vertex The vertex whose adjacent nodes are to be found
	 * @return a list of adjacent nodes of vertex
	 */
	public List<Character> getAdjacent(char vertex){
		List<Character> adjacentVertices = new LinkedList<>();
		int vertexInt = getIntArrayIndexFromChar(vertex);
		for(int j=0;j<nVertex;j++){
			if(! inRange(vertexInt, 0, nVertex) ) throw new IllegalArgumentException();
			if(adjacentMatrix[vertexInt][j] > 0)
				adjacentVertices.add(getCharFromInt(j));  
		}
		return adjacentVertices;
	}
	


	
}
