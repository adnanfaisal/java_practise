package category06.designpattern.builderpattern;

public enum BookType {
	FICTION, NON_FICTION;
}
