package category02.datastructure;

import org.junit.Test;

 class BSTreeRecursive {

	class Node {
		int value;
		Node left = null;
		Node right = null;
		
		public Node(int v){
			value = v;
		}
	}
	
	Node root;
	
	public BSTreeRecursive(int x){
		root = new Node(x);
	}
	//--------- Insert a Node ----------------------//
	public void insert(int x){
		root = insert(root,x);
	}	
	public Node insert(Node node, int x){
		if(node == null){
			return new Node(x);
		}
		else if (x > node.value) node.right = insert(node.right,x);
		else if (x < node.value) node.left = insert(node.left,x);
		else;
		return node;
	}
	
	//----------- Find a Node-------------------------//
	public boolean find(int x){
		return find(root,x);
	}
	public boolean find (Node node, int x){
		if(node != null && node.value == x) return true;
		else if( node != null && x > node.value) return find(node.right, x);
		else if (node != null && x < node.value) return find(node.left,x);
		else return false;
	}
	
	
	//-----------Remove a Node----------------------//
	public void remove(int x){
		remove(root,x);
	}
	
	public void remove(Node node, int x){
		
	}
	public void displayTree(){
		displayTreeRecursive(root);
	}
	
	private void displayTreeRecursive(Node node){
		if (node != null){
			displayTreeRecursive(node.left);
			System.out.print(node.value + " ");
			displayTreeRecursive(node.right);				
	}
		
}
}
public class BSTreeRecursiveApp{
	
	public BSTreeRecursiveApp(){}
	
	@Test
	public void BinaryTreeTest(){
	BSTreeRecursive btree = new BSTreeRecursive(10);
	btree.insert(20);
	btree.insert(2);
	btree.insert(13);
	btree.insert(5);
	btree.insert(7);
	btree.insert(15);
	btree.insert(17);
	btree.insert(21);	
	btree.insert(3);
	btree.insert(7);
	btree.insert(21);
	btree.displayTree();
	
	System.out.println("Found 21 ? " + btree.find(21));
	System.out.println("Found 11 ? " + btree.find(11));
	System.out.println("Found 3 ? " + btree.find(3));
	System.out.println("Found 10 ? " + btree.find(10));
		
	
	}
}
