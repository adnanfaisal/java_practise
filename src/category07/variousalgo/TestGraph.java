package category07.variousalgo;

import org.junit.Test;

public class TestGraph {
	@Test
	public void makeGraph(){
		
		Graph graph = new Graph(5);
		graph.addEdge('a', 'b');
		graph.addEdge('a', 'c');
		graph.addEdge('b', 'd');
		graph.addEdge('c', 'd');
		graph.addEdge('d', 'e');
		
	// Output should be like this
	/*	    A B C D E 
		  A 0 1 1 0 0 
		  B 1 0 0 1 0 
		  C 1 0 0 1 0 
		  D 0 1 1 0 1 
		  E 0 0 0 1 0 
	*/	
		System.out.println(graph.toString());
		System.out.println(graph.getAdjacent('b'));
		
	}
}
