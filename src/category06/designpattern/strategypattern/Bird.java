package category06.designpattern.strategypattern;

public class Bird extends Animal {

	public Bird(String name) {
		super(name);
		flyType = new ItFlies();
	}

}
