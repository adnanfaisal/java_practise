package category02.datastructure;

import org.junit.Test;


 class ANode<T> {

	private T value;
	private ANode<T> next;
	
	public ANode(T value) {
		this(value,null);
	}	
	
	public ANode(T value, ANode<T> next) {
		this.value = value;
		this.next = next;
	}	
	
	public T getValue() {
		return value;
	} 
	
	public ANode<T> getNext(){
		return next;
	}
	


	
}

public class LinkedListGeneric{
	
	@Test
	public void testLL(){
		
		
	}
	
}
