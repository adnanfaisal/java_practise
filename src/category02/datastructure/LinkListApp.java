// From Java Data Struct and Algo book page 211
package category02.datastructure;

import static org.junit.Assert.*;

import org.junit.Test;

class Link{
	int value; /* keeping package protection level for the sake of simplicity */
	Link next;
	
	public Link(int value){
		this.value = value;
		next = null;
	}
	
	public void displayLink(){
		System.out.println("value = " + value);
	}
	
}


class LinkList {
	Link first;
	
	public LinkList(){
		first = null;
	}
	
	public void insertFirst(int value){
		Link link = new Link(value);
		link.next = first;
		first = link;
	}
	
	public Link deleteFirst(){
		Link deletedLink = first;
		if(!isEmpty()) first = first.next;
		return deletedLink;
	}
	
	public boolean find(int value){
		Link element = first;
		while(element!=null){
			if(element.value == value) return true;
			else element = element.next;
		}
		return false;
	}
	
	public boolean delete(int value){
		Link element = first;
		Link previousElement = null;
		while(element != null){
			if(element.value == value && element == first) {
				deleteFirst();
				return true;
			}
			else if (element.value == value && element.next == null) {
				previousElement.next = null;
				return true;
			}
			else if(element.value == value){
				previousElement.next = element.next;
				element.next = null;
				return true;
			}
			previousElement = element;
			element = element.next;
		}
		return false;
	}
	
	public boolean isEmpty(){
		return (first == null);
	}
	
	
	public void displayList(){
		Link element = first;
		while(element != null){
			element.displayLink();
			element = element.next;
		}
	}
}

public class LinkListApp{
	
	public LinkListApp(){}
	
	@Test
	public void LinkListTest(){
		LinkList ll = new LinkList();
		ll.insertFirst(10);
		ll.insertFirst(20);
		ll.insertFirst(30);
		ll.displayList();
		
		assertTrue(ll.find(10));
		assertFalse(ll.find(50));
			
		ll.delete(20);
		ll.displayList();
		}
}
