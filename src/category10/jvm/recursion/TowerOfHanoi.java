package category10.jvm.recursion;

import org.junit.Test;

public class TowerOfHanoi {

	public void solve(int nDisk, char from, char to, char via){
		if(nDisk==1) {
			System.out.println("Move disk " + nDisk + " from " + from + " to " + to + " via " + via);
			return;
		}
		else{
			solve(nDisk-1,from, via, to);
			System.out.println("Move disk " + nDisk +  " from " + from + " to " + to + " via " + via);
			solve(nDisk-1, via, to, from);
		}		
	}
	
	@Test
	public void TestToH(){
		new TowerOfHanoi().solve(2,'A','C','B');
	}
	
}
