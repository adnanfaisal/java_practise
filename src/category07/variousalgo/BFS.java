package category07.variousalgo;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

public class BFS {
	

	Queue<Character> queue = new ArrayDeque<>();
	Graph graph;
	boolean visited[];
	
	public  BFS(int nVertex){
		graph = new Graph(nVertex);
		visited = new boolean[nVertex];		
	}
	
	public String doBreadthFirstSearch(){
		return doBreadthFirstSearch('A');
	}
	
	/*
	 * @param src the node from where traversal begins
	 * @return the list of nodes visited as a String
	 */
	public String doBreadthFirstSearch(Character src){
		StringBuilder sb = new StringBuilder();
		int srcInt = Graph.getIntArrayIndexFromChar(src);
		visited[srcInt] = true;
		
		while(src!=null)
		{ 
		
			sb.append(Character.toUpperCase(src) + " ");	
			List<Character> adjacent = graph.getAdjacent(src);		
			for (char c:adjacent){
				if(visited[Graph.getIntArrayIndexFromChar(c)]!= true){
					queue.add(c);
					System.out.println("Adjacent of  " +  Character.toUpperCase(src) + " is " + c);
					srcInt = Graph.getIntArrayIndexFromChar(c);
					visited[srcInt] = true;			
				}
			}
			src = queue.poll();
		}
		return new String(sb);
		
	}
	
	
	
	public Graph getGraph() {
		return graph;
	}



	public void setGraph(Graph graph) {
		this.graph = graph;
	}



	
	
	
}
