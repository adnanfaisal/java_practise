package category02.datastructure;

import java.util.ArrayList;
import java.util.List;

public class SetOfStacks<T> {

	List<LimitedStack<T>> stackOfStacks = new ArrayList<>();
	
	public SetOfStacks(int s){
		LimitedStack<T> newLimitedStack = new LimitedStack<>(s); 
		stackOfStacks.add(newLimitedStack);
	}
	
	//incomplete
	public void push(T elem){
		stackOfStacks.get(stackOfStacks.size()-1);
	}
	
	
}