package category06.designpattern.strategypattern;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Animal {
	protected FlyBehaviour flyType; // there can be many animal subclasses that fly
	private String name;
	public Animal(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String fly(){
		return flyType.fly();
	}
	
	public void setFlyBehaviour(FlyBehaviour flyType){
		this.flyType = flyType;
	}
}


