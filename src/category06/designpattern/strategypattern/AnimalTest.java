package category06.designpattern.strategypattern;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AnimalTest{
@Test
public void StrategyPatternTest(){
	
	Bird julie = new Bird("julie");
	Dog john = new Dog("john");
	assertEquals("julie: I fly",julie.getName() + ": " + julie.fly());
	assertEquals("john: I can't fly",john.getName() + ": " + john.fly());
	
	john.setFlyBehaviour(new ItFlies());
	assertEquals("john: I fly",john.getName() + ": " + john.fly());
	
		
}
}