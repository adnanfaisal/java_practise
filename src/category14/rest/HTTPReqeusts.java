package category14.rest;

import static org.junit.Assert.assertEquals;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class HTTPReqeusts {
	// @Test
	    public void makeBareHttpRequest() throws IOException {
	        final URL url = new URL("http", "www.google.ca", "/");
	        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setRequestMethod("GET");

	        final InputStream responseInputStream = connection.getInputStream();

	        final int responseCode = connection.getResponseCode();
	        @SuppressWarnings("deprecation")
			final String response = IOUtils.toString(responseInputStream);

	        responseInputStream.close();

	        assertEquals(200, responseCode);
	        System.out.printf("Response received: [%s]%n", response);
	    }
	 

	    @SuppressWarnings("deprecation")
		@Test
	    public void makeApacheHttpClientRequest() throws IOException {
	        @SuppressWarnings("deprecation")
			final DefaultHttpClient client = new DefaultHttpClient();
	        HttpGet get = new HttpGet("http://en.wikipedia.org/");

	        final HttpResponse response = client.execute(get);
	        final int responseCode = response.getStatusLine().getStatusCode();

	        final HttpEntity entity = response.getEntity();
	        final InputStream responseBody = entity.getContent();

	        assertEquals(200, responseCode);
	        System.out.printf("Response received: [%s]%n", IOUtils.toString(responseBody));
	    }

}
