package adnan.leetcode.algorithm;
/**
 * Problem: 
 * You are given two linked lists representing two non-negative numbers. 
 * The digits are stored in reverse order and each of their nodes contain a 
 * single digit. Add the two numbers and return it as a linked list.
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 * 
 * @author Adnan
 */
import static org.junit.Assert.*;

import org.junit.Test;

// Given: Definition for singly-linked list.
class ListNode {
	int val;
	ListNode next;
	ListNode(int x) { val = x; }
}

public class P2_AddTwoNumbers {

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
	        
	        ListNode rootNode = new ListNode(-1);
	        ListNode lastNode = new ListNode(-1);
	        
	        int l1val;
	        int l2val;
	        int carry=0;
	     
	        while(l1 != null || l2!=null){
	            l1val = l2val = 0;
	            if(l1!=null) {
	                l1val = l1.val;
	                l1 = l1.next;
	            }
	            if(l2!=null) {
	                l2val = l2.val;
	                l2 = l2.next;
	            }
	            int sumDigit = (l1val + l2val + carry) % 10;
	            carry = (l1val + l2val + carry) / 10;
	          
	            
	            if (rootNode.val == -1) {
	                rootNode.val = sumDigit;
	                lastNode = rootNode;
	            }
	            else{
	                ListNode sumNode = new ListNode(sumDigit);
	                lastNode.next = sumNode;
	                lastNode = sumNode;
	            }
	            
	        }
	        
	        if(carry!=0) {
	             ListNode finalCarry = new ListNode(carry);
	             lastNode.next = finalCarry;
	             lastNode = finalCarry;
	        }
	        
	        return rootNode;        
	    }

	@Test
	public void test1(){
		
		ListNode l1a = new ListNode(2);
		ListNode l1b = new ListNode(4);
		ListNode l1c = new ListNode(3);
		l1a.next = l1b;
		l1b.next = l1c;
		
		ListNode l2a = new ListNode(5);
		ListNode l2b = new ListNode(6);
		ListNode l2c = new ListNode(4);
		l2a.next = l2b;
		l2b.next = l2c;
		ListNode sum = new P2_AddTwoNumbers().addTwoNumbers(l1a, l2a);
		
		ListNode digit1 = new ListNode(7);
		ListNode digit2 = new ListNode(0);
		ListNode digit3 = new ListNode(8);
		
		assertEquals(sum.val,digit1.val);
		sum = sum.next;
		
		assertEquals(sum.val,digit2.val);
		sum = sum.next;
		
		assertEquals(sum.val,digit3.val);
		
		
	}
}
