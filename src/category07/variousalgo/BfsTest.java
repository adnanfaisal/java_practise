package category07.variousalgo;

import org.junit.Test;

public class BfsTest {

	/**
	 * Example taken from venugopal's lect-1 of bfs (youtube)
	 * This graph is shown at the beginning
	 */
//	@Test
	public void printBfsPath(){
		BFS bfs = new BFS(5);
		Graph graph = bfs.getGraph();
		graph.addEdge('a', 'b');
		graph.addEdge('a', 'c');
		graph.addEdge('b', 'd');
		graph.addEdge('c', 'd');
		graph.addEdge('d', 'e');
		
		
		System.out.println(graph);
		System.out.println(bfs.doBreadthFirstSearch('B'));
		
	}
	
	/**
	 *  Example taken from venugopal's lect-1 of bfs (youtube)
	 * This graph is shown in 9:42 min
	 */
	@Test
	public void printBfsPath2(){
		BFS bfs = new BFS(7);
		Graph graph = bfs.getGraph();
		graph.addEdge('a', 'b');
		graph.addEdge('a', 'c');
		graph.addEdge('b', 'd');
		graph.addEdge('c', 'd');
		graph.addEdge('d', 'e');
		
		graph.addEdge('b', 'e');
		graph.addEdge('d', 'f');
		graph.addEdge('f', 'g');
		graph.addEdge('a', 'g');
		graph.addEdge('g', 'c');
		
		
		
		System.out.println(graph);
		System.out.println(bfs.doBreadthFirstSearch('b'));
		
	}
}
