package category01.sort;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class BinarySearch {

	public static boolean doBinarySearch(List<Integer> sortedInput, int item){
		
		if (sortedInput == null || sortedInput.isEmpty()) return false;
		
		
		int middle = sortedInput.size() / 2;
		int middleitem = sortedInput.get(middle);
		
		if(item == middleitem) return true;
		
		if(item < middleitem) return doBinarySearch(sortedInput.subList(0, middle),item);
		else return doBinarySearch(sortedInput.subList(middle+1,sortedInput.size()),item);
				
	}
	
	@Test
	public void checkBinarySearch(){
		
			ArrayList<Integer> values = new ArrayList<Integer>();
			
			values.add(-9);			
			values.add(3);
			values.add(10);
			values.add(50);
			
			assertEquals(doBinarySearch(values,3),true);
			assertEquals(doBinarySearch(values,5),false);
			
			
		
	}


}
