package category10.jvm.recursion;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class FibonacciNCached {

	private Map<Long,Long> fibCache = new HashMap<>();
	
	
	public long findFibN(long n){
		
		if(n<0) throw new IllegalArgumentException();
		
		fibCache.put(0L,0L);
		fibCache.put(1L,1L);
		
		return findFib(n);
	}
	
	private long findFib(long n){
		
		if(fibCache.containsKey(n)) return fibCache.get(n);
		
		long fibN = findFib(n-1) + findFib(n-2);
		fibCache.put(n, fibN);
		return fibN;		
	}
	
	@Test
	public void fibTest(){
		long start = System.currentTimeMillis();
		assertEquals(1134903170,findFibN(45));
		long end = System.currentTimeMillis();
		System.out.println("difference  = " + (end - start)   + "ms");
	}
	
	
}
