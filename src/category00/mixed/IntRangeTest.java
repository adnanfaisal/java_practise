package category00.mixed;

import org.junit.Test;

public class IntRangeTest {

	@Test
	public void intTest(){
		int max = Integer.MAX_VALUE;
		int min = Integer.MIN_VALUE;
		
		System.out.println("max = " + max);
		System.out.println("min = " + min);
		
		System.out.println("After increment and decrement ...");
		
		System.out.println("max = " + ++max);
		System.out.println("min = " + --min);
		
		System.out.println("Bytes -----");
		byte maxb = Byte.MAX_VALUE;
		byte minb = Byte.MIN_VALUE;
		
		System.out.println("max = " + maxb + " Binary = " + Integer.toBinaryString(maxb) );
		System.out.println("min = " + minb + " Binary = " + Integer.toBinaryString(minb) );
		
		System.out.println("After increment and decrement ...");
		
		System.out.println("max = " + ++maxb + " Binary = " + Integer.toBinaryString(maxb) );
		System.out.println("min = " + --minb + " Binary = " + Integer.toBinaryString(minb) );
		
	}
}
