package adnan.leetcode.algorithm;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/***
 * Problem 4 of Leetcode
 * @author adnan
 * @version 06 Oct 2016
 */
public class P4_MedianOfTwoSortedArrays {
	
	    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
	        int totalLength = nums1.length + nums2.length;
	        
	        //special case
	        if (totalLength == 0) return 0.0;
	        if(totalLength == 1) return handleOneElemCaseSpecially(nums1,nums2);
	        
	        int middle = 0; // middleIndex, which is supposed to be the median's index
	        double median = 0;
	        int lastValue = 0; // the lastValue is needed when the median is the avg of the two middle elements
	 
	   
	        middle = totalLength / 2;
	        int iptr = 0;
	        int jptr = 0;
	        
	        for (int i=0; i< totalLength; i++){
	            
	            // if an array is exhausted, the pointer of other array should move forward
	            if(iptr == nums1.length){
	                lastValue = nums2[jptr];
	                jptr++;
	            }
	            else if(jptr == nums2.length){
	                lastValue = nums1[iptr];
	                iptr++;
	            }
	                  
	            // move forward the pointer of the lesser element array
	            else if(nums1[iptr] < nums2[jptr]){
	                    lastValue = nums1[iptr];
	                    iptr++;
	            }
	            else{
	                    lastValue = nums2[jptr];
	                    jptr++;
	            }
	            
	            // check if median is found
	            if(iptr + jptr == middle){
	                int curValue = 0;
	                if(totalLength % 2 == 0){         // median is average of the two middle elements
	                    if(nums1.length == iptr) curValue = nums2[jptr]; // if iptr crossed boundary than mid elem must be from jptr
	                    else if (nums2.length == jptr) curValue = nums1[iptr];
	                    else // if no array is exahusted then the minimum of the two pointed values should be the middle element 
	                        curValue = Math.min(nums1[iptr],nums2[jptr]);
	                    
	                    median = (lastValue + curValue) * 1.0 / 2.0;
	                }
	                else{ // no. of elements are odd
	                    if(iptr == nums1.length) median = nums2[jptr];
	                    else if (jptr == nums2.length) median = nums1[iptr];
	                    else median = Math.min(nums1[iptr],nums2[jptr]);
	                }
	                return median;
	            }
	        } // end of for
	        return median;
	    }
	    
	    public double handleOneElemCaseSpecially(int[] nums1, int[] nums2){
	        if(nums1.length == 0) return 1.0*nums2[0];
	        else return 1.0*nums1[0];
	    }
	    
	    @Test
		public void test1(){

			double median =new P4_MedianOfTwoSortedArrays().findMedianSortedArrays(new int[]{1,3}, new int[]{2});
			assertEquals(median,2.0,0.001); // third param is epsilon telling how much error you tolerate		
	    }
	    
	    @Test
		public void test2(){

			double median =new P4_MedianOfTwoSortedArrays().findMedianSortedArrays(new int[]{1,2}, new int[]{3,4});
			assertEquals(median,2.5,0.001); // third param is epsilon telling how much error you tolerate		
	    }
	    
	    
	    
	
}
