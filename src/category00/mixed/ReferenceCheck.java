package category00.mixed;


public class ReferenceCheck {


	public void howReferenceWorks(){
		Box b = new Box(10);
		Box bb = new Box(20);
		
		Box b1 = b;
		Box b2 = b1;
		b1 = bb;
		System.out.println("b1: " + b1.getNumber());
		System.out.println("b2: " + b2.getNumber());		
	}
	
	public static void main(String args[]){
		new ReferenceCheck().howReferenceWorks();
	}
	
}
