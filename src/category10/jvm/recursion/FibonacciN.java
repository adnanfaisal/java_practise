package category10.jvm.recursion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FibonacciN {

	public static long findFibN(long n){
		
		if(n<0)  throw new IllegalArgumentException();
		if(n==0) return 0;
		if (n==1) return 1;
		else return findFibN(n-1) + findFibN(n-2);
	}
	
	@Test
	public void fibTest(){
		long start = System.currentTimeMillis();
		assertEquals(1134903170,findFibN(45));
		long end = System.currentTimeMillis();
		System.out.println("difference  = " + (end - start)   + "ms");
	}
	
}
