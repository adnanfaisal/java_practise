package category01.sort;

import java.util.ArrayList;
import java.util.List;

public class MergeSort {

	public static List<Integer> mergeSort(List<Integer> input){
		
		if(input.size()<2) return input;
		
		Integer middle = input.size()/2;
		List<Integer> left = input.subList(0, middle);
		List<Integer> right = input.subList(middle, input.size());
		
		return merge(mergeSort(left),mergeSort(right));
	}
	
	private static List<Integer> merge(final List<Integer> left, final List<Integer> right){
		
		
		int leftptr = 0;
		int rightptr = 0;
		
		final List<Integer> sortedList = new ArrayList<Integer>(left.size() + right.size());
		
		while(leftptr<left.size() && rightptr<right.size()){
			if(left.get(leftptr) < right.get(rightptr)){
				sortedList.add(left.get(leftptr));
				leftptr++;
			}
			else {
				sortedList.add(right.get(rightptr));
				rightptr++;
			}
		}
		
		while(leftptr<left.size()){
			sortedList.add(left.get(leftptr++));
		
		}
		
		while(rightptr<right.size()){
			sortedList.add(right.get(rightptr++));
		}
		
		return sortedList;
	}
	
	public static void main(String []args){
		ArrayList<Integer> values = new ArrayList<Integer>();
		values.add(10);
		values.add(-9);
		values.add(50);
		values.add(3);
		ArrayList<Integer> sortedValues = (ArrayList<Integer>) mergeSort(values);
		for(Integer in:sortedValues)
			System.out.print(in+ " ");
		
	}
}
