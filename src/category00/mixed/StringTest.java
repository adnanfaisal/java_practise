package category00.mixed;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringTest {

	@Test
	public void StringVsBufferVsBuilder(){
	String s1 = "This is a string";
	s1.concat(" updated");
	assertEquals(s1,"This is a string"); // s1 is not updated since it is not mutable
	
	//StringBuffer is thread safe, String Builder and String are not
	StringBuilder sbl = new StringBuilder(s1);
	System.out.println(sbl);
	sbl.append(" updated");
	assertEquals(sbl.toString(),"This is a string updated"); // sb1 is updated

	
	
	}
}
