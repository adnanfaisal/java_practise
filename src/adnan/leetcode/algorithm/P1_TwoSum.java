package adnan.leetcode.algorithm;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * Problem:
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution.
 * Example:
 * Given nums = [2, 7, 11, 15], target = 9,
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 * @author adnan
 * 
 */
public class P1_TwoSum {
	public int[] twoSum(int[] nums, int target) {
        int[] indices = new int[2];
        
        Map<Integer,Integer> map = new HashMap<>();
        
        for(int i=0;i<nums.length;i++){
            int complement = target - nums[i];
            if(map.containsKey(complement)){
                indices[1] = i;
                indices[0] = map.get(complement);
                return indices;
            }
            map.put(nums[i],i);
        }
        return indices;       
    }
	
	@Test
	public void test1(){
		int[] indices = new P1_TwoSum().twoSum(new int[] {3,2,4}, 6);
		assertTrue(Arrays.equals(indices,new int[] {1,2}));
	}
}
