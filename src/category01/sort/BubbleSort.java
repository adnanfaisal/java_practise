package category01.sort;

public class BubbleSort {
	
	public static int[] bubbleSort(int values[]){
		boolean swapped;
		do{
			swapped = false;
			for(int i =0;i<values.length-1;i++){
				if(values[i]>values[i+1]){
					int temp = values[i];
					values[i] = values[i+1];
					values[i+1] = temp;
					swapped = true;
				}
			}			
		}while (swapped); 
		return values;
		
	}
	
	public static void main(String []args){
		int[] vals = {10,2,3,-1,88,1044};
		bubbleSort(vals);
		for(int i=0;i<vals.length;i++)
			System.out.print(vals[i] + " ");
		
	}

}
