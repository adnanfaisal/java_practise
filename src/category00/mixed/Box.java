package category00.mixed;

public class Box {
	Integer number;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Box(Integer number) {
		super();
		this.number = number;
	}
	
	
	
}
