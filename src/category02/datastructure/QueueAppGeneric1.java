package category02.datastructure;

import org.junit.Test;

class Queue1 <T extends Number>{
	T data[];
	int front = 0;
	int rear = -1;
	int nItems = 0;
	
	@SuppressWarnings("unchecked")
	public Queue1(int size){
		data = (T[]) new Number[size];
	}
	
	public void insert(T value) throws Exception{		
		if (nItems == data.length){
			throw new Exception("Queue overflow");
		}
		else{
			rear = (rear + 1) % data.length;
			data[rear] = value;
			nItems++;
			System.out.println(value + " inserted.");
		}
	}
	
	public T remove() throws Exception{
		if(nItems == 0) {
			throw new Exception("Queue is empty");
		}
		T temp = data[front];
		front = (front + 1) % data.length;
		nItems--;
		System.out.println(temp + " removed.");
		return temp;
	}
	
	public void display(){
		
		for(int count = 0, i = front; count < nItems; count++){
			System.out.print(data[i] + " ");
			i = (i + 1) % data.length;
		}
		System.out.println("");
	}
	
	public boolean isFull(){
		
		if (nItems == data.length) return true;
		else return false;
	}
	
}
public class QueueAppGeneric1 {
	
	public QueueAppGeneric1(){}
	
	@Test
	public void QueueTestInteger() {
		Queue1<Integer> queue = new Queue1<>(3);
		try{
			queue.insert(10);
			queue.insert(20);
			queue.insert(30);
		}catch(Exception ex){
			ex.printStackTrace();
		}
			queue.display();
		try{
		queue.insert(40);
		queue.display();
		}catch(Exception ex) { System.err.println(ex);}
		
		try{
		queue.remove();
		queue.display(); 
		} catch(Exception ex) { System.err.println(ex);}
		
		try{
		queue.remove();
		queue.remove();
		queue.remove();
		queue.display();
		} catch(Exception ex) { System.err.println(ex);}				
	}
	
	@Test
	public void QueueTestFloat() {
		Queue1<Double> queue = new Queue1<>(3);
		try{
			queue.insert(10.5);
			queue.insert(20.0);
			queue.insert(30.3);
		}catch(Exception ex){
			ex.printStackTrace();
		}
			queue.display();
		try{
		queue.insert(40.9937);
		queue.display();
		}catch(Exception ex) { System.err.println(ex);}
		
		try{
		queue.remove();
		queue.display(); 
		} catch(Exception ex) { System.err.println(ex);}
		
		try{
		queue.remove();
		queue.remove();
		queue.remove();
		queue.display();
		} catch(Exception ex) { System.err.println(ex);}				
	}

}
